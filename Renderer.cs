﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Media.Media3D;
using System.Windows.Forms;

namespace Editor3D
{
    public class Renderer
    {
        public enum RenderMode
        {
            Wireframe,
            Solid
        }
        
        public IEnumerable<IPrimitive> primitives;
        public Camera camera;
        public Bitmap bitmap;
        public RenderMode renderMode;
        public Color ambientColor;
        public Color boundaryColor;

        public Renderer(IEnumerable<IPrimitive> primitives, Camera camera, Bitmap bitmap, RenderMode renderMode,
            Color ambientColor, Color boundaryColor)
        {
            this.primitives = primitives;
            this.camera = camera;
            this.bitmap = bitmap;
            this.renderMode = renderMode;
            this.ambientColor = ambientColor;
            this.boundaryColor = boundaryColor;
        }

        public void Render()
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            List<IPrimitive> transformedPrimitives = Transform(primitives, camera);
            List<List<Point2D>> rasterizedPrimitives = Rasterize(transformedPrimitives);
            Draw(rasterizedPrimitives, bitmapData);
            bitmap.UnlockBits(bitmapData);
        }
        
        private class Point2D
        {
            public int x;
            public int y;
            public double z;
            public Color color;

            public Point2D(int x, int y, double z, Color color)
            {
                this.x = x;
                this.y = y;
                this.z = z;
                this.color = color;
            }

            public static explicit operator Point2D(Point3D point)
                => new Point2D((int) point.X, (int) point.Y, point.Z, point.Color);
        }

        private List<IPrimitive> Transform(IEnumerable<IPrimitive> primitives, Camera camera)
        {
            List<IPrimitive> transformedPrimitives = new List<IPrimitive>();
            foreach (IPrimitive primitive in primitives)
                if (primitive is Point3D point)
                    transformedPrimitives.Add(point
                        .Transform(camera.Projection)
                        .NormalizedToDisplay(bitmap.Width, bitmap.Height));
                else if (primitive is Line line)
                    transformedPrimitives.Add(new Line(
                        line.A
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height),
                        line.B
                            .Transform(camera.Projection)
                            .NormalizedToDisplay(bitmap.Width, bitmap.Height),
                        line.Color));
                else if (primitive is Facet facet)
                    transformedPrimitives.Add(new Facet(
                        facet.points.Select(
                            facetPoint => facetPoint
                                .Transform(camera.Projection)
                                .NormalizedToDisplay(bitmap.Width, bitmap.Height)).ToList(),
                        facet.Color));
                else if (primitive is Polyhedron polyhedron)
                    transformedPrimitives.Add(new Polyhedron(
                        polyhedron.points.Select(
                            facetPoint => facetPoint
                                .Transform(camera.Projection)
                                .NormalizedToDisplay(bitmap.Width, bitmap.Height)).ToList(),
                        polyhedron.facets.Select(
                            polyhedronFacet => new Facet(polyhedronFacet.points.Select(
                                polyhedronPoint => polyhedronPoint
                                    .Transform(camera.Projection)
                                    .NormalizedToDisplay(bitmap.Width, bitmap.Height)).ToList(),
                                polyhedronFacet.Color)).ToList(),
                        polyhedron.name));
            return transformedPrimitives;
        }

        private List<List<Point2D>> Rasterize(List<IPrimitive> primitives)
        {
            List<List<Point2D>> rasterizedPrimitives = new List<List<Point2D>>();
            foreach (IPrimitive primitive in primitives)
                if (primitive is Point3D point)
                {
                    if (point.Z >= 0)
                        rasterizedPrimitives.Add(new List<Point2D>(new[] { (Point2D)point }));
                }
                else if (primitive is Line line)
                    rasterizedPrimitives.Add(RasterizeLine(line, line.Color));
                else if (primitive is Facet facet)
                    rasterizedPrimitives.Add(RasterizeFacet(facet, facet.Color, boundaryColor));
                else if (primitive is Polyhedron polyhedron) {
                    bool isConvex = !polyhedron.name.Equals("Rotation Figure");
                    Vector3D directionVector = new Vector3D(0, 0, -1);
                    int count = 0;
                    foreach (Facet polyhedronFacet in polyhedron.facets)
                    {
						if (polyhedron.name.Equals("Plot"))
						{
							rasterizedPrimitives.Add(RasterizeFacet(polyhedronFacet, polyhedronFacet.Color, boundaryColor));
							continue;
						}

                        Vector3D normal;
                        if (isConvex)
                        {
                            normal = Geometry.NormalVector(polyhedronFacet, polyhedron.Center);
                        }
                        else
                        {
                            if (count == polyhedron.facets.Count - 1)
                            {
                                normal = Geometry.NormalVector(polyhedronFacet);
                            }
                            else
                            {
                                normal = -1 * Geometry.NormalVector(polyhedronFacet);
                            }
                        }

                        if (Vector3D.AngleBetween(directionVector, normal) <= 90)
                        {
                            rasterizedPrimitives.Add(RasterizeFacet(polyhedronFacet, polyhedronFacet.Color, boundaryColor));
                        }
                        count++;
                    }
                }
            return rasterizedPrimitives;
        }

        private void Draw(List<List<Point2D>> rasterizedPrimitives, BitmapData bitmapData)
        {
            Clear(bitmapData);
            if (renderMode == RenderMode.Wireframe)
            {
                foreach (List<Point2D> pointList in rasterizedPrimitives)
                    foreach (Point2D point in pointList)
                        if (0 <= point.x && point.x < bitmapData.Width && 0 <= point.y && point.y < bitmapData.Height)
                            SetPixel(bitmapData, point.x, point.y, point.color);
            }
            else
            {
                double[,] zBuffer = new double[bitmapData.Width, bitmapData.Height];
                for (int y = 0; y < bitmapData.Height; y++)
                    for (int x = 0; x < bitmapData.Width; x++)
                        zBuffer[x, y] = double.MaxValue;
                foreach (List<Point2D> pointList in rasterizedPrimitives)
                    foreach (Point2D point in pointList)
                        if (0 <= point.x && point.x < bitmapData.Width && 0 <= point.y && point.y < bitmapData.Height)
                            if (point.z < zBuffer[point.x, point.y])
                            {
                                SetPixel(bitmapData, point.x, point.y, point.color);
                                zBuffer[point.x, point.y] = point.z;
                            }
            }
        }

        private List<Point2D> RasterizeLine(Line line, Color color)
        {
            List<Point2D> points = new List<Point2D>();
            Point2D point1, point2;
            if ((int) line.A.X < (int) line.B.X)
            {
                point1 = (Point2D) line.A;
                point2 = (Point2D) line.B;
            }
            else
            {
                point1 = (Point2D) line.B;
                point2 = (Point2D) line.A;
            }
            
            int dx = point2.x - point1.x;
            int dy = point2.y - point1.y;
            double gradient;
            if (dx == 0 && dy > 0)
                gradient = 1;
            else if (dx == 0 && dy < 0)
                gradient = -1;
            else if (dx == 0 && dy == 0)
                return new List<Point2D>(new[] {point1});
            else
                gradient = (double)dy / dx;
            
            if (gradient >= 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y <= point2.y)
                {
                    double t = (double) (y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y++;
                }
            }
            else if (0 <= gradient && gradient < 1)
            {
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y++;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else if (-1 < gradient && gradient < 0)
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dy - dx;
                while (x <= point2.x)
                {
                    double t = (double)(x - point1.x) / (point2.x - point1.x);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dy;
                    else
                    {
                        y--;
                        d += 2 * (dy - dx);
                    }
                    x++;
                }
            }
            else
            {
                dy = -dy;
                int x = point1.x;
                int y = point1.y;
                double d = 2 * dx - dy;
                while (y >= point2.y)
                {
                    double t = (double) (y - point1.y) / (point2.y - point1.y);
                    double z = t * (point2.z - point1.z) + point1.z;
                    if (z >= 0)
                        points.Add(new Point2D(x, y, z, color));
                    if (d < 0)
                        d += 2 * dx;
                    else
                    {
                        x++;
                        d += 2 * (dx - dy);
                    }
                    y--;
                }
            }

            return points;
        }
        
        private List<Point2D> RasterizeFacet(Facet facet, Color facetColor, Color boundaryColor)
        {   List<Point2D> boundary = new List<Point2D>();
            for (int i = 0; i < facet.points.Count; i++)
                boundary.AddRange(RasterizeLine(new Line(
                    facet.points[i],
                    facet.points[(i + 1) % facet.points.Count]),
                    (renderMode == RenderMode.Solid ? boundaryColor : facetColor)));           

            if (renderMode == RenderMode.Wireframe)
                return boundary;
            
            List<List<Point2D>> rows = boundary
                .GroupBy(point => point.y)
                .Select(group => group
                    .OrderBy(point => point.x)
                    .ToList())
                .ToList();
            
            List<Point2D> points = new List<Point2D>();
            foreach (List<Point2D> rowPoints in rows)
            {
                int y = rowPoints[0].y;
                if (rowPoints[0].z >= 0)
                    points.Add(rowPoints[0]);
                for (int i = 1; i < rowPoints.Count; i++)
                {
                    if (rowPoints[i].x - rowPoints[i - 1].x > 1)
                        for (int x = rowPoints[i - 1].x + 1; x <= rowPoints[i].x - 1; x++)
                        {
                            double t = (double) (x - rowPoints[i - 1].x) / (rowPoints[i].x - rowPoints[i - 1].x);
                            double z = t * (rowPoints[i].z - rowPoints[i - 1].z) + rowPoints[i - 1].z;
                            if (z >= 0)
                                points.Add(new Point2D(x, y, z, facetColor));
                        }
                    if (rowPoints[i].z >= 0)
                        points.Add(rowPoints[i]);
                }
            }

            return points;
        }
        
        private void Clear(BitmapData bitmapData)
        {
            for (int y = 0; y < bitmapData.Height; y++)
                for (int x = 0; x < bitmapData.Width; x++)
                    SetPixel(bitmapData, x, y, ambientColor);
        }

        private unsafe void SetPixel(BitmapData bitmapData, int x, int y, Color color)
        {
            if (x < 0 || x >= bitmapData.Width || y < 0 || y >= bitmapData.Height)
                return;
            byte* bitmapPointer = (byte*)bitmapData.Scan0.ToPointer();
            bitmapPointer[bitmapData.Stride * y + 3 * x] = color.B;
            bitmapPointer[bitmapData.Stride * y + 3 * x + 1] = color.G;
            bitmapPointer[bitmapData.Stride * y + 3 * x + 2] = color.R;
        }
    }
}