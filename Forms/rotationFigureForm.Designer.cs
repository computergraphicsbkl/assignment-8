﻿namespace Editor3D
{
    partial class RotationFigureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generatrixTextBox = new System.Windows.Forms.TextBox();
            this.generatrixLabel = new System.Windows.Forms.Label();
            this.axisLabel = new System.Windows.Forms.Label();
            this.xRadioButton = new System.Windows.Forms.RadioButton();
            this.yRadioButton = new System.Windows.Forms.RadioButton();
            this.zRadioButton = new System.Windows.Forms.RadioButton();
            this.splitsLabel = new System.Windows.Forms.Label();
            this.splitsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.createModelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.splitsNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // generatrixTextBox
            // 
            this.generatrixTextBox.Location = new System.Drawing.Point(35, 44);
            this.generatrixTextBox.Multiline = true;
            this.generatrixTextBox.Name = "generatrixTextBox";
            this.generatrixTextBox.Size = new System.Drawing.Size(259, 157);
            this.generatrixTextBox.TabIndex = 0;
            // 
            // generatrixLabel
            // 
            this.generatrixLabel.AutoSize = true;
            this.generatrixLabel.Location = new System.Drawing.Point(15, 15);
            this.generatrixLabel.Name = "generatrixLabel";
            this.generatrixLabel.Size = new System.Drawing.Size(242, 15);
            this.generatrixLabel.TabIndex = 1;
            this.generatrixLabel.Text = "1. Задайте точки образующей  в виде X Y Z";
            // 
            // axisLabel
            // 
            this.axisLabel.AutoSize = true;
            this.axisLabel.Location = new System.Drawing.Point(19, 215);
            this.axisLabel.Name = "axisLabel";
            this.axisLabel.Size = new System.Drawing.Size(143, 15);
            this.axisLabel.TabIndex = 64;
            this.axisLabel.Text = "2. Задайте ось вращения";
            // 
            // xRadioButton
            // 
            this.xRadioButton.AutoSize = true;
            this.xRadioButton.Checked = true;
            this.xRadioButton.Location = new System.Drawing.Point(35, 245);
            this.xRadioButton.Name = "xRadioButton";
            this.xRadioButton.Size = new System.Drawing.Size(32, 19);
            this.xRadioButton.TabIndex = 65;
            this.xRadioButton.TabStop = true;
            this.xRadioButton.Text = "X";
            this.xRadioButton.UseVisualStyleBackColor = true;
            // 
            // yRadioButton
            // 
            this.yRadioButton.AutoSize = true;
            this.yRadioButton.Location = new System.Drawing.Point(91, 245);
            this.yRadioButton.Name = "yRadioButton";
            this.yRadioButton.Size = new System.Drawing.Size(32, 19);
            this.yRadioButton.TabIndex = 66;
            this.yRadioButton.Text = "Y";
            this.yRadioButton.UseVisualStyleBackColor = true;
            // 
            // zRadioButton
            // 
            this.zRadioButton.AutoSize = true;
            this.zRadioButton.Location = new System.Drawing.Point(147, 245);
            this.zRadioButton.Name = "zRadioButton";
            this.zRadioButton.Size = new System.Drawing.Size(32, 19);
            this.zRadioButton.TabIndex = 67;
            this.zRadioButton.Text = "Z";
            this.zRadioButton.UseVisualStyleBackColor = true;
            // 
            // splitsLabel
            // 
            this.splitsLabel.AutoSize = true;
            this.splitsLabel.Location = new System.Drawing.Point(22, 283);
            this.splitsLabel.Name = "splitsLabel";
            this.splitsLabel.Size = new System.Drawing.Size(197, 15);
            this.splitsLabel.TabIndex = 68;
            this.splitsLabel.Text = "3. Укажите количество разбиений:";
            // 
            // splitsNumericUpDown
            // 
            this.splitsNumericUpDown.Location = new System.Drawing.Point(35, 312);
            this.splitsNumericUpDown.Name = "splitsNumericUpDown";
            this.splitsNumericUpDown.Size = new System.Drawing.Size(50, 23);
            this.splitsNumericUpDown.TabIndex = 69;
            // 
            // createModelButton
            // 
            this.createModelButton.Location = new System.Drawing.Point(103, 345);
            this.createModelButton.Name = "createModelButton";
            this.createModelButton.Size = new System.Drawing.Size(117, 27);
            this.createModelButton.TabIndex = 70;
            this.createModelButton.Text = "Создать модель";
            this.createModelButton.UseVisualStyleBackColor = true;
            this.createModelButton.Click += new System.EventHandler(this.createModelButton_Click);
            // 
            // RotationFigureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(323, 383);
            this.Controls.Add(this.createModelButton);
            this.Controls.Add(this.splitsNumericUpDown);
            this.Controls.Add(this.splitsLabel);
            this.Controls.Add(this.xRadioButton);
            this.Controls.Add(this.yRadioButton);
            this.Controls.Add(this.zRadioButton);
            this.Controls.Add(this.axisLabel);
            this.Controls.Add(this.generatrixLabel);
            this.Controls.Add(this.generatrixTextBox);
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "RotationFigureForm";
            ((System.ComponentModel.ISupportInitialize) (this.splitsNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label generatrixLabel;
        private System.Windows.Forms.Label axisLabel;
        private System.Windows.Forms.Label splitsLabel;
        private System.Windows.Forms.Button createModelButton;
        public System.Windows.Forms.NumericUpDown splitsNumericUpDown;
        public System.Windows.Forms.RadioButton zRadioButton;
        public System.Windows.Forms.RadioButton yRadioButton;
        public System.Windows.Forms.RadioButton xRadioButton;
        public System.Windows.Forms.TextBox generatrixTextBox;
    }
}